#!/bin/bash

clear
echo "Starting Desktop Configuration..."
sleep 2

clear
echo "Getting fastest mirrors..."
echo
sudo sed -i 's/#Color/Color/' /etc/pacman.conf
sudo sed -i 's/#ParallelDownloads/ParallelDownloads/' /etc/pacman.conf
sudo systemctl start reflector.service
cat /etc/pacman.d/mirrorlist
sleep 2

clear
echo "Refreshing keyrings..."
echo
sudo pacman -Sy
sudo pacman -S archlinux-keyring
sudo pacman -S --needed base-devel git
sleep 2

clear
echo "Installing Paru..."
echo
cd ~/Downloads
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
cd ~/sway-install
sleep 2

clear
echo "Installing sway and related applications..."
echo
# paru -S network-manager-applet blueman pavucontrol sway swaybg swayidle swaylock-effects-git swayimg waybar rofi-llbonn-wayland-git mako \
#        arc-gtk-theme papirus-icon-theme noto-fonts-emoji noto-fonts terminus-font nautilus file-roller \
#        gnome-disk-utility python-i3ipc python-requests pamixer polkit-gnome imagemagick jq gedit python-pip \
#        kitty brightnessctl gammastep geoclue autotiling python-nautilus gvfs-smb nwg-bar nwg-wrapper \
#        ttf-nerd-fonts-symbols-1000-em nautilus-open-any-terminal grim slurp wl-clipboard simple-scan \
#        libreoffice-still libreoffice-still-en-gb hunspell hunspell-en_ca hyphen hyphen-en libmythes \
#        mythes-en aurutils plymouth nerd-fonts-git ntfs-3g ark neovim xwayland bluez bluez-utils blueman \
#        seatd xorg-xwayland xorg-xlsclients qt5-wayland qt6-wayland glfw-wayland udiskie-dmenu-git wl-clipboard-rs-git

paru -S pipewire pipewire-alsa pipewire-audio pipewire-jack pipewire-pulse gst-plugin-pipewire wireplumber networkmanager network-manager-applet bluez bluez-utils blueman \
sddm-git qt5-wayland qt6-wayland qt5-quickcontrols qt5-quickcontrols2 qt5-graphicaleffects sway-git dunst rofi-lbonn-wayland-git waybar swww swaylock-effects-git wlogout \
grimblast-git slurp swappy cliphist polkit-kde-agent xdg-desktop-portal-hyprland pacman-contrib python-pyamdgpuinfo parallel jq imagemagick qt5-imageformats ffmpegthumbs \
brightnessctl pavucontrol pamixer nwg-look kvantum qt5ct firefox kitty neofetch dolphin visual-studio-code-bin ark zsh eza oh-my-zsh-git zsh-theme-powerlevel10k-git pokemon-colorscripts-git \
xorg-xwayland xorg-xlsclients qt5-wayland qt6-wayland glfw-wayland udiskie-dmenu-git wl-clipboard-rs-git

sleep 2

clear
echo "Installing fcitx5 and Chinese support..."
echo
sudo pacman -S adobe-source-han-serif-cn-fonts wqy-zenhei noto-fonts noto-fonts-cjk noto-fonts-emoji noto-fonts-extra nerd-fonts
paru -S fcitx5-im fcitx5-chinese-addons fcitx5-rime fcitx5-qt fcitx5-gtk fcitx5-lua \
       fcitx5-pinyin-zhwiki rime-pinyin-zhwiki rime-cloverpinyin sway-im fcitx5-material-color
sudo mkdir ~/.local/share/fcitx5/rime # 创建 rime 目录
sudo tee -a ~/.local/share/fcitx5/rime/default.custom.yaml << EOF
patch:
  "menu/page_size": 8
  schema_list:
    - schema: clover
EOF
sleep 2

clear
echo "Installing code support..."
echo
sudo pacman -S maven neovide docker docker-compose neofetch
paru -S jdk-lts jdk8-adoptopenjdk
       
sleep 2

clear
echo "Installing others..."
echo
sudo pacman -S vivaldi firefox
paru -S visual-studio-code-bin obsidian jetbrains-toolbox dingtalk-bin linuxqq telegram-desktop-bin windterm-bin musicfox-bin
       
sleep 2

clear
echo "Applying configuration..."
echo
echo "Copying configuration files..."
cp -R .config/* $HOME/.config/
# cp .bashrc $HOME/
sudo cp 09-timezone /etc/NetworkManager/dispatcher.d/
sudo cp 90-monitor.conf /etc/X11/xorg.conf.d/
sudo mkdir /etc/sddm.conf.d
sudo cp sddm.conf /etc/sddm.conf.d/
sudo cp theme.conf /usr/share/sddm/themes/sugar-candy/
sudo sed -i 's/HOOKS=(base systemd/HOOKS=(base systemd sd-plymouth/' /etc/mkinitcpio.conf
sudo sed -i 's/rw quiet/rw quiet splash vt.global_cursor_default=0/' /boot/loader/entries/arch.conf
sudo plymouth-set-default-theme -R spinfinity
sleep 2

echo
echo "Applying gsettings..."
sudo glib-compile-schemas /usr/share/glib-2.0/schemas
gsettings set com.github.stunkymonkey.nautilus-open-any-terminal terminal foot
gsettings set com.github.stunkymonkey.nautilus-open-any-terminal keybindings '<Ctrl><Alt>t'
gsettings set com.github.stunkymonkey.nautilus-open-any-terminal new-tab true
sleep 2

# echo
# echo "Add archlinuxcn mirrors..."
# sudo tee -a /etc/pacman.conf << EOF 
# [archlinuxcn]
# # 中国科学技术大学开源镜像站
# Server = https://mirrors.ustc.edu.cn/archlinuxcn/$arch
# # 清华大学开源软件镜像站
# Server = https://mirrors.tuna.tsinghua.edu.cn/archlinuxcn/$arch
# # 哈尔滨工业大学开源镜像站
# Server = https://mirrors.hit.edu.cn/archlinuxcn/$arch
# # 华为开源镜像站
# Server = https://repo.huaweicloud.com/archlinuxcn/$arch
# EOF
# sudo pacman -Syyu
# sudo pacman -S archlinuxcn-keyring
# sudo pacman -Syyu
# sleep 2

echo
echo "Config Fcitx5 & Wayland & Chinese..."
sudo tee -a /etc/environment << EOF 
GTK_IM_MODULE DEFAULT=fcitx
QT_IM_MODULE  DEFAULT=fcitx
XMODIFIERS    DEFAULT=\@im=fcitx
INPUT_METHOD  DEFAULT=fcitx
SDL_IM_MODULE DEFAULT=fcitx
GLFW_IM_MODULE DEFAULT=ibus
XIM=fcitx
XIM_PROGRAM=fcitx

GDK_BACKEND=wayland
QT_QPA_PLATFORM=wayland
CLUTTER_BACKEND=wayland
SDL_VIDEODRIVER=wayland

MOZ_ENABLE_WAYLAND=1
EOF

sleep 5

echo
echo "Shell Config..."
sudo pacman -S zsh zsh-autosuggestions zsh-syntax-highlighting zsh-completions
sudo pacman -S autojump
chsh -l # 查看安装了哪些 Shell
chsh -s /usr/bin/zsh # 修改当前账户的默认 Shell
sudo tee -a ~/.zshrc << EOF 
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/autojump/autojump.zsh
EOF
source ~/.zshrc
sleep 5

echo
echo "TLP Config..."
sudo pacman -S tlp tlp-rdw
paru -S tlpui
sudo systemctl enable tlp.service
sudo systemctl enable NetworkManager-dispatcher.service
sudo systemctl mask systemd-rfkill.service # 屏蔽以下服务以避免冲突，确保 TLP 无线设备的开关选项可以正确运行
sudo systemctl mask systemd-rfkill.socket
sleep 5

echo
echo "Enabling Something..."
sudo systemctl enable sddm.service
sudo systemctl enable --now bluetooth
sudo systemctl enable --now seatd
sleep 5

clear
echo "Installation complete, rebooting..."
sleep 2
reboot
