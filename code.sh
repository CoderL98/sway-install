#!/bin/bash
 
# 用户名
username="test-likunyao"
# 密码
password="Lixiao980213."
 
# 将项目的地址变成一个数组
projectArr=(
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-vehiclemodel/busreg-vehiclemodel-query.git"
    # AJ05车型查询平台-车辆定型应用
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/veh-unity-insci.git"
    # AJ01-监管接入平台-全国车险承保交强险服务
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-vehiclemodel/busreg-vehiclemodel-external.git"
    # 车型查询平台对外服务（车惠达、七炅、中交兴路、精友新能源等）
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/busreg-domain-model.git"
    # AJ01-监管接入平台-领域模型
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/veh-unity-insbi.git"
    # AJ01-监管接入平台-承保全国商业险服务
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/busreg-local-vehicle.git"
    # AJ01-监管接入平台-地方车险服务
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/regulatory-scheduler.git"
    # AJ01-监管接入平台-定时任务调度服务
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/busreg-local-accident.git"
    # AJ01-监管接入平台-地方意健险服务
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/businsurance-regulatory-common.git"
    # AJ01-监管接入平台-公共组件
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/busreg-vehicle-claim.git"
    # 监管接入-全国车险银保信理赔登记服务
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-vehiclemodel/busreg-vehiclemodel-web.git"
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/businsurance-regulatory-platform-starter.git"
    # AJ01-监管接入平台-公共组件-platform
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/busreg-vehicle-insurance.git"
    # AJ01-监管接入平台-保单接入子域-全国车险承保服务
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/businsurance-regulatory-oceanbase-starter.git"
    # AJ01-监管接入平台-公共组件-oceanbase
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/all-veh-insurance.git"
    # AJ01-监管接入平台-全部地方平台车承保批改
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/busreg-electronply-upload.git"
    # 电子保单归档
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/busreg-all-regulatory-insurance-mq.git"
    # AJ01-监管接入消息监听服务(承保+理赔)
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/regulatory-backstage.git"
    # AJ01-监管接入平台-运维管理系统-后端项目
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/regulatory-access-management.git"
    # AJ01-监管接入平台-回溯中心-决策管理-任务管理
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-vehiclemodel/busreg-vehiclemodel-ubi.git"
    # AJ05-车型查询平台-ubi缓存服务
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/all-acci-insurance.git"
    # AJ01-监管接入平台-保单接入子域-地方意外险服务
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-vehiclemodel/busreg-vehiclemodel-save.git"
    # AJ05车型查询平台-车辆车型信息保存应用
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/businsurance-regulatory-mongodb-starter.git"
    # AJ01-监管接入平台-公共组件-mongodb
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/busreg-code-generation.git"
    # AJ01-监管接入平台-自定义注解式代码生成器
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/busreg-exception.git"
    # AJ01-监管接入平台-自定义异常
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/busreg-custom.git"
    # AJ01-监管接入平台-自定义注解/异常
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/busreg-integration.git"
    # AJ01-监管接入平台-集成服务
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/businsurance-regulatory-log-starter.git"
    # 日志组建
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/all-busplatform-baffle.git"
    # AJ01-监管接入平台-全险种监管挡板服务
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/businsurance-regulatory-convert-starter.git"
    # AJ01-监管接入平台-转换类
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/veh-cache-ins.git"
    # AJ01-监管接入平台-车险缓存服务
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/busreg-cache.git"
    # AJ01-监管接入平台-配置支撑子域-缓存中心
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/busreg-acci-sd-riskwarning.git"
    # AJ01监管接入-山东意外险风险信息预警服务
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/businsurance-regulatory-redis-starter.git"
    # AJ01-监管接入平台-公共组件-redis
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/regulatory-fusing-warn.git"
    # AJ01-监管接入平台-配置支撑子域-监控熔断预警服务
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-ocr/ocr-management-platform-check.git"
    # OCR平台核验服务
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/businsurance-regulatory-sofamq-starter.git"
    # AJ01-监管接入平台-sofamq公共组件
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/all-platform-showcase.git"
    # AJ01-监管接入平台-接入平台清单
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/veh-unity-clmbi.git"
    # AJ01-监管接入平台-全国平台商业险理赔服务
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/veh-region-claims.git"
    # AJ01-监管接入-车险理赔地方监管/交管对接
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/acci-sd-riskwarning-register.git"
    # AJ01-监管接入-山东意外险登记推送服务
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/veh-unity-clmci.git"
    # AJ01-监管接入-全国交强险理赔
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/generic-policy-check.git"
    # AJ01-监管接入平台-新一代保单验真服务
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/veh-unity-shie.git"
    # AJ01-监管接入平台-接入上海保交所新能源车险
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/all-realname-regist.git"
    # AJ01-监管接入-老系统实名查验登记
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/businsurance-regulatory-filter-starter.git"
    # AJ01-监管接入-公共组件-filter
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/busreg-eolinker-api.git"
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/businsurance-regulatory-suggest-time.git"
    # AJ01-监管接入平台-获取推荐时间组件
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/busreg-personalized-rule-control.git"
    # 个性化规则管控
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/acci-sc-nocomplaint.git"
    # 四川意外险反欺诈系统
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/businsurance-regulatory-caffeine-starter.git"
    # AJ01-监管接入平台-Caffeine公共组件
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/regulatory-decision-factor-management.git"
    # AJ01-监管接入平台-配置支撑子域-决策管理-前端
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/regulatory-task-management.git"
    # 监管接入平台-运维后端管理服务
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/regulatory-decision-analysis.git"
    # 监管决策分析-前端
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/businsurance-regulatory-redisson-starter.git"
    # Redisson公共组件
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/regulatory-regulation-fusing.git"
    # 监管熔断项目-前端
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-vehiclemodel/vrhicle-claim-data-es.git"
    # AJ05-车型查询平台-监管整改-理赔数据库存储es
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/all-realname-verify-register.git"
    # AJ01-监管接入平台-新一代实名查验登记系统
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/bj-acci-health-insurance.git"
    # 北京意健险
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/all-prod-claimmq.git"
    # AJ01监管接入-履约消息消费服务
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/all-prod-insurance-mq.git"
    # AJ01-监管接入平台-保单接入子域-消息处理服务
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/regulatory-operations-management.git"
    # AJ01-监管接入平台-运维管理系统
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/regulatory-information-platform.git"
    # AJ01-监管接入平台-配置支撑子域-平台信息管理-前端
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/acci-unity-riskmanage.git"
    # AJ01银保信意健险风险管理系统平台服务
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/veh-fee-regist.git"
    # 全国费用子系统服务
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-vehiclemodel/busreg-vehiclemodel-chd.git"
    # AJ05-车型查询平台-车慧达查询服务
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/businsurance-regulatory-front.git"
    # 监管接入平台-前端项目
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/veh-gd-trafficcon.git"
    # 广东车辆数据综合服务平台
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/veh-js-platform.git"
    # 江苏实名缴费认证&中介平台
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/veh-sz-insplatform.git"
    # 深圳双录平台系统
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/veh-sh-insplatform.git"
    # AJ01监管接入平台上海平台承保服务
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/channel-intergration.git"
    # 对接渠道统一****
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/veh-sc-vehmodelquey.git"
    # 四川交管平台机动车车辆信息查询与比对校验接口
    "http://code.ant-test.res.cloud.cic.inter/cictest/busplatform-regulatory/regulatory-cache.git"
    # AJ01-监管接入平台-缓存中心
)
 
# 当前所在目录
current=$PWD
 
# 遍历数据取值
for i in ${projectArr[*]}
do
	cd $current
	# 字符串截取
    # 从左往右扫描并截取最后一次出现'/'的右侧字符，即经过这次截取会变成'projectA.git'
    dir=${i##*/}
    # 从右往左扫描并截取第一次出现'.'的左侧字符，即经过这次截取会变成'projectA'
    dir=${dir%%.*}
    # echo $dir
 
    # 判断项目的目录是否存在，不存在则克隆，存在则拉取
    if [ ! -d $dir ];then
    	# echo $i
        # 字符串替换，将地址添加上账号密码，即'http://'替换成'http://username:password@'
        projectAddr=${i/http:\/\//http://$username:$password@}
 
        echo '*********正在clone' $dir '项目...********'
        git clone $projectAddr
        echo '*********clone' $dir '项目成功！*********'
    fi
    
    echo -e "\n*****正在拉取" $dir "项目...****"    
    cd $dir
    git pull
    echo -e "*****拉取" $dir "项目完成！*****\n"
done